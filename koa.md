1、创建npm配置文件

```
npm init
```

2、创建项目文件

```
// 在项目根目录创建
app.js   // 项目入口文件
start.js // 项目启动文件
```

3、下载依赖

```
npm  install  koa@next --save

npm install babel-core --save-dev
npm install babel-polyfill --save-dev
npm install babel-preset-es2015 --save-dev
npm install babel-preset-stage-3 --save-dev
```

4、编写文件

```
// start.js
require("babel-core/register")(
    {
        presets: ['stage-3','es2015']
    }
);

require("babel-polyfill");

require("./app.js");
```

  换行

```
// app.js
const Koa = require('koa');
const app = new Koa();

app.use(async (ctx, next) => {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
});


// response
app.use(ctx => {
  ctx.body = 'Hello Koa5555';
});

app.listen(3000);
```

5、启动项目

```
// 首先下载
npm install nodemon  -g
```

  换行

```
nodemon start
```

  换行

```
打开浏览器，访问页面
```

**远程搭建mongoDB**

在mongo atals 里面创建一个500MB的免费数据库，然后在本地安装mongoDB  

```shell
npm install mongoose --save
```

在mongoDB集群的管理界面点击connect  选择第二个选项(connect your application)，点进去

![image-20210822140814446](C:\Users\wys\AppData\Local\Temp\360zip$Temp\360$0\images\image-20210822140814446.png)

```shell
mongodb+srv://root:<password>@cluster0.oylf1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
```

就可以拿到这个url，只是把 <password>换成你自己设置的密码就行了。

然后在app.js里面写：

```javascript
mongoose.connect("mongodb+srv://root:caonima1998@cluster0.oylf1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
```

这里的密码我已经换了哈。

然后使用nodemon运行程序 ，不过事先需要在全局安装nodemon

```shell
npm install -g nodemon
```

然后直接执行

```shell
nodemon
```

就可以了。

另外，可以把URL写在配置文件里面

```javascript
module.exports={
    mongoURL: 'mongodb+srv://root:caonima1998@cluster0.oylf1.mongodb.net/test?retryWrites=true&w=majority&useUnifiedTopology=true'
}
```

然后在app.js里面引过来

```javascript
const db=require("./config/keys").mongoURL
```

之后需要用到url的地方，全部使用db这个变量来替代就可以了

**首先定义实例模式**

这个其实就相当于pojo层，把数据表中的属性封装成一个实体类，然后在程序中通过对这个实体类的操作来操作数据库。

```javascript
const mongoose=require("mongoose")    //引入依赖项（这个mongoose之前已经装好了）
const schema=mongoose.Schema        
const UsrSchema=new schema({          //定义实体类的数据结构
    name:{
        type : String,
        required : true
    },
    email:{
        type : String,
        required : true
    },
    date:{
        type : Date,
        default : Date.now
    }
})
//把这个定义好的模式暴露出去，其他程序就拿得到了
module.exports=Usr=mongoose.model("usrs",UsrSchema)
```



**然后编写控制程序**

这一层相当于controller层  访问服务： ip+端口(配置的是5000)+路径别名（下面有写怎么绑定别名）+get函数的第一个参数

```javascript
const Router=require("koa-router")
const Usr = require("../../models/Usr")
const router=new Router()
router.get("/test",async ctx=>{            
   ctx.status=200      //状态正常
   ctx.body={msg:"hello,usr!"}
})

router.post("/register", async ctx=>{     
   console.log(ctx.request.body)
   const findRes=await Usr.find({email:ctx.request.body.email})
   if(findRes.length>0){
      ctx.status=500
      ctx.body={email: '邮箱已被使用'}
   }
   else{
    const newUsr=new Usr({
      name : ctx.request.body.name,
      email : ctx.request.body.email
    })
    console.log(newUsr)
    await newUsr.save().then(user=>{
       ctx.body=user
    }).catch(err=>{
       console.log(err)
    })
   } 
})
```

然后在app.js里面添加

```javascript
const router=new Router()
//服务实际地址
const usr=require("./routers/api/usr")
//router.use这个东西相当于给实际服务绑定个访问用的路径，当输入localhost:5000/api/usr/test的时候，就会访问usr这个变量所定位的文件中的/test服务
router.use('/api/usr',usr)
```

![image-20210827144800831](C:\Users\wys\AppData\Local\Temp\360zip$Temp\360(\images\image-20210827144800831.png)

**数据库插入操作**

```javascript
//如果在数据库中存在相同的邮箱，则注册失败，不让其插入
const findRes=await Usr.find({email:ctx.request.body.email}) //归根结底是schema在帮你找
   if(findRes.length>0){
      ctx.status=500
      ctx.body={email: '邮箱已被使用'}
   }

//拿到一个定义好的模式(pojo)，并将post请求传过来的参数赋值进去
const newUsr=new Usr({
      name : ctx.request.body.name,
      email : ctx.request.body.email
    })
//将该pojo实例写入数据库  
await newUsr.save()

//这一段程序的await都是表示同步操作，以免多用户一起注册时邮箱出现并发问题（比如邮箱注册重复）
```

![image-20210827145700232](C:\Users\wys\AppData\Local\Temp\360zip$Temp\360(\images\image-20210827145700232.png)

在postman里面选post操作，然后在body里面写要传的参数。

在mongo Atlas数据库里面就看得到插入的数据了

![image-20210827150701708](C:\Users\wys\AppData\Local\Temp\360zip$Temp\360(\images\image-20210827150701708.png)



**下面对增删改查做个汇总**

## 增查改删(CRUD)

所有的参数都是以`JSON对象`形式传入。

### 增(C)

> [Model.create(doc(s), [callback\])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.create)

```lua
var doc = ({
    title:  "Mongoose",
    author: "L",
    body:   "Documents are instances of out model. Creating them and saving to the database is easy",
    comments: [{ body: "It's very cool! Thanks a lot!", date: "2014.07.28" }],
    hidden: false,
    meta: {
        votes: 100,
        favs:  99
    }
};

blogModel.create(doc, function(err, docs){
    if(err) console.log(err);
    console.log('保存成功：' + docs);
});
```

> [Model#save([options\], [options.safe], [options.validateBeforeSave], [fn])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model-save)

```javascript
var blogEntity = new blogModel({
    title:  "Mongoose",
    author: "L",
    body:   "Documents are instances of out model. Creating them and saving to the database is easy",
    comments: [{ body: "It's very cool! Thanks a lot!", date: "2014.07.28" }],
    hidden: false,
    meta: {
        votes: 100,
        favs:  99
    }
});

blogEntity.save(function(err, docs){
    if(err) console.log(err);
    console.log('保存成功：' + docs);
});
```

> [Model.insertMany(doc(s), [options\], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.insertMany)

多条数据插入，将多条数据一次性插入，相对于循环使用`create`保存会更加快。

```hsp
blogModel.insertMany([
    {title: "mongoose1", author: "L"}, 
    {title: "mongoose2", author: "L"}
    ], function(err, docs){
        if(err) console.log(err);
        console.log('保存成功：' + docs);
});
```

### 查(R)

> [Model.find(conditions, [projection\], [options], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.find)

`conditions`：查询条件；`projection`：控制返回的字段；`options`：控制选项；`callback`：回调函数。

```hsp
blogModel.find({title: "Mongoose", meta.votes: 100}, {title: 1, author: 1, body: 1}, function(err, docs){
    if(err) console.log(err);
    console.log('查询结果：' + docs);
})
```

查询“title”标题为“Mongoose”，并且“meta”中“votes”字段值为“100”的记录，返回仅返回“title”、“author”、“body”三个字段的数据。

> [Model.findOne([conditions\], [projection], [options], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.findOne)

`conditions`：查询条件；`projection`：控制返回的字段；`options`：控制选项；`callback`：回调函数。
只返回第一个查询记录。

> [Model.findById(id, [projection\], [options], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.findById)

`id`：指定`_id`的值；`projection`：控制返回的字段；`options`：控制选项；`callback`：回调函数。

### 改(U)

> [Model.update(conditions, doc, [options\], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.update)

`conditions`：查询条件；`doc`：需要修改的数据，不能修改主键（`_id`）；`options`：控制选项；`callback`：回调函数，返回的是受影响的行数。
`options`有以下选项：
　　safe (boolean)： 默认为true。安全模式。
　　upsert (boolean)： 默认为false。如果不存在则创建新记录。
　　multi (boolean)： 默认为false。是否更新多个查询记录。
　　runValidators： 如果值为true，执行Validation验证。
　　setDefaultsOnInsert： 如果upsert选项为true，在新建时插入文档定义的默认值。
　　strict (boolean)： 以strict模式进行更新。
　　overwrite (boolean)： 默认为false。禁用update-only模式，允许覆盖记录。

```javascript
blogModel.update({title: "Mongoose"}, {author: "L"}, {multi: true}, function(err, docs){
    if(err) console.log(err);
    console.log('更改成功：' + docs);
})
```

以上代码先查询“title”为“Mongoose”的数据，然后将它的“author”修改为“L”，“multi”为true允许更新多条查询记录。

> [Model.updateMany(conditions, doc, [options\], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.updateMany)

一次更新多条

> [Model.updateOne(conditions, doc, [options\], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.updateOne)

一次更新一条

> [Model.findByIdAndUpdate(id, [update\], [options], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.findByIdAndUpdate)

`id`：指定`_id`的值；`update`：需要修改的数据；`options`控制选项；`callback`回调函数。
`options`有以下选项：
　　new： bool - 默认为false。返回修改后的数据。
　　upsert： bool - 默认为false。如果不存在则创建记录。
　　runValidators： 如果值为true，执行Validation验证。
　　setDefaultsOnInsert： 如果upsert选项为true，在新建时插入文档定义的默认值。
　　sort： 如果有多个查询条件，按顺序进行查询更新。
　　select： 设置数据的返回。

> [Model.findOneAndUpdate([conditions\], [update], [options], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.findOneAndUpdate)

`conditions`：查询条件；`update`：需要修改的数据；`options`控制选项；`callback`回调函数。
`options`有以下选项：
　　new： bool - 默认为false。返回修改后的数据。
　　upsert： bool - 默认为false。如果不存在则创建记录。
　　fields： {Object|String} - 选择字段。类似.select(fields).findOneAndUpdate()。
　　maxTimeMS： 查询用时上限。
　　sort： 如果有多个查询条件，按顺序进行查询更新。
　　runValidators： 如果值为true，执行Validation验证。
　　setDefaultsOnInsert： 如果upsert选项为true，在新建时插入文档定义的默认值。
　　passRawResult： 如果为真，将原始结果作为回调函数第三个参数。

### 删(D)

> [Model.remove(conditions, [callback\])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.remove)

```lua
blogModel.remove({author: "L"}, function(err, docs){
    if(err) console.log(err);
    console.log('删除成功：' + docs);
})
```

删除“author”值为“L”的记录。

> [Model.findByIdAndRemove(id, [options\], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.findByIdAndRemove)

`id`：指定`_id`的值；`update`：需要修改的数据；`options`控制选项；`callback`回调函数。
`options`有以下选项：
　　sort： 如果有多个查询条件，按顺序进行查询更新。
　　select： 设置数据的返回。

> [Model.findOneAndRemove(conditions, [options\], [callback])](https://link.segmentfault.com/?url=http%3A%2F%2Fmongoosejs.com%2Fdocs%2Fapi.html%23model_Model.findOneAndRemove)

`conditions`：查询条件；`update`：需要修改的数据；`options`控制选项；`callback`回调函数。
`options`有以下选项：
　　sort： 如果有多个查询条件，按顺序进行查询更新。
　　maxTimeMS： 查询用时上限。
　　select： 设置数据的返回。